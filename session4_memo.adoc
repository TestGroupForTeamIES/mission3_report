:toc: left
:sectnums:
:toc-title: 目次
:toclevels: 1
:icons: font
:author: チームIES　羅
:revnumber: 1.0

:path_Install_Google_Chrome: https://qiita.com/shadowhat/items/af6b973df43d75abfe8e

= Session4、管理業務

== 前提知識
=== 復習
====
* Linux：マルチタスク、マルチユーザーで実行可能
* ユーザーアカウント(ログインIDと相当)
* アプリが使用するアカウントの保存場所 +
1．/etc/passwd +
2．/etc/shadow +
====
=== 操作
====
* cd /etc
* vi passwd +
* sudo vi shadow +
====
=== passwdの中身
====
* ユーザー名：パスワード(Xの場合はShadowで管理)：ユーザーID：グループID：コメント欄：ホームDIR：デフォルトシェル +
EX：root:x:0:0:root:/root:/bin/bash
※/bin/bash/nologinの場合はログイン不可となる
====

<<<

== アクセス制御
=== ユーザーとグループ
====
* 一般ユーザー、特権ユーザー(root)
* グループ：複数のユーザーの纏め
====
=== パーミッション
====
* アクセス権限許可設定
====
=== アクセス許可
====
* 読み書き(r)、4
* 書き込み(w)、2
* 実行(x)、1
====
=== chown、chowmの使用
====
* sudo chown ＜ユーザー＞:＜グループ＞ ＜ファイル名＞ +
EX：sudo chown root:root 123.txt +

* chowm ＜権限＞ ＜ファイル＞ +
EX：sudo chown o+w 123.txt
====

<<<

== パッケージ管理システム
====
* プロントエンドツール（apt、yumなど）
* パッケージ管理ツール（dpkg、rpmなど) 
====
=== Debian/Ubuntuパッケージの導入
====
* ダウンロード系 +
apt-get

* GUIでダウンロード +
Ubuntuのソフトウェアセンター

* ダウンロード、インストールを独立実行 +
1．debでファイルをダウンロード +
2．dpkgコマンドでインストール +
====

=== Google Chromeの場合
====
* リポジトリの追加 +
apt-get install google-chrome-state

* ブラウザでファイルを取得 +
dpkg -i google-chrome-stable_current_amd64.deb
====


=== Debain系のパッケージ管理
====
* dpkgパッケージファイル名のフォーマット +
＜パッケージ名＞_＜バージョン＞_＜リリース.アーキテクチャ＞.deb

* EX：google chromeの場合 +
1．sudo dpkg google-chrome-stable_current_amd64.deb +
2．sudo apt-get install libappindicator7(依存関係が必要な場合) +
3．起動ファイル /usr/bin/google-chrme/stable +
====

<<<

=== dpkgコマンド
====
* dpkg ＜アクション｜オプション＞ ＜dpkgパッケージファイル＞ +
-i、インストール  +
-r、アンインストール +
-P、アンインストール（設定も削除） +
-I、アンインストールパッケージの表示 +
-L、アンインストールパッケージのファイル一覧表示 +
-s、パッケージ情報を表示 +
-S、ファイルの件さ、表示 +
-E、同じバージョンなら上書きしない +
-G、新しいバージョンがインストールされる場合上書きしない
====


=== RedHat系(CentOS)のパッケージ導入
====
* ダウンロード、インストール +
yum コマンド

* ダウンロード、インストールを独立実行 +
1．rpmファイルをブラウザでダウンロード +
2．rpmコマンドでインストール

* GUI
====

=== Yumコマンド
====
* Yellowdog Updater Modified
* Aptと似てる
* 基本設定：/etc/yum.conf
* リポジトリ:/etc/yum.repos.d
* Google Chromeのインストール +
yum install google-chrome-stable

* Option +
install ：インストール +
remove ：アンインストール +
check-update ：アップデートリストの表示 +
update ：パッケージのアップデート +
list ：パッケージ一覧の表示 +
repolist ：登録リポジトリの一覧表示 +
search ＜キーワード＞：キーワードのパッケージを検査 +

* yumdownloader：ダウンロードのみ行うコマンド
====

<<<

=== RPM
====
* パッケージ名の例: Google Chrome +
google-chrome-stable_current_x86_64.rpm +

* RPMコマンドのオプション +
-i ：インストール +
-U ：アップグレード（なければインストール） +
-F ：アップグレード（インストール済みの場合） +
-h ：進行状況の表示 +
--force ：強制インストール +
--nodeps ：パッケージ依存菅関係を無視 +
-e ：アンケートされたファイルの表示 +
-q ：問い合わせ +
-l ：インストールファイルの表示 +
-a ：全情報の表示 +
-p ：パッケージの指定 +
-f ：指定ファイルのパッケージ表示 +
====


== 共有ライブラリ
====
* Cコンパイラー +
gcc ＜.cファイル＞

* 実行ファイルの実行 +
./＜実行ファイル＞

* ライプラリー参照のチェックコマンド +
ldd ＜実行ファイル＞
====

== ジョブのスケジュール
====
* 定期実行一覧の表示 +
crontab -e

* crontabの書式：分、時、日、月、曜日、実行ジョブ
====

<<<


== 練習：CentOSにGoogle Chromeをインストール
====
* リポジトリの設定 +
vi /etc/yum.repos.d/google.chrome.repo

* 内容を編集（下記内容を追加） +
[google-chrome] +
name=google-chrome +
baseurl=http://dl.google.com/linux/chrome/rpm/stable/$basearch +
enabled=1 +
gpgcheck=1 +
gpgkey=https://dl-ssl.google.com/linux/linux_signing_key.pub +

* 情報反映 +
yum update

* インストール +
yum install google-chrome-stable

* 参考ウエブサイト： +
link:{path_Install_Google_Chrome}[CentOS7にChromeをインストール]
====



