:toc: left
:sectnums:
:toc-title: 目次
:toclevels: 1
:icons: font
:author: チームIES　羅
:revnumber: 1.0

:path_session2: https://gitlab.com/TestGroupForTeamIES/mission3_report/raw/master/session2_memo.pdf?inline=false
:path_session3: https://gitlab.com/TestGroupForTeamIES/mission3_report/raw/master/session3_memo.pdf?inline=false
:path_session4: https://gitlab.com/TestGroupForTeamIES/mission3_report/raw/master/session4_memo.pdf?inline=false
:path_session5: https://gitlab.com/TestGroupForTeamIES/mission3_report/raw/master/session5_memo.pdf?inline=false
:path_session7: https://gitlab.com/TestGroupForTeamIES/mission3_report/raw/master/session7_memo.pdf?inline=false
:path_5day: https://www.udemy.com/5dayslinux/

= Mission 3: Virtual Box, Linux


== 要件
====
1．4日間で、link:{path_5day}[はじめての Linux 入門（LPIC Level1対応） | Udemy]を学習すること +
2．この中で扱うコード、まとめのAsciidocなどは、全てGitlabのレポジトリで共有管理すること +
3．学んだこと、詰まったところなどのアドバイスを、Asciidocで残すこと +
====


== セッションタイトル
|===
1．Linuxの基礎知識 +

2．学習環境の構築 +
メモ：link:{path_session2}[こちら] +

3．GNUとUnixのコマンド（Linuxの基本操作） +
メモ：link:{path_session3}[こちら] +

4．管理業務 +
メモ：link:{path_session4}[こちら] +

5．ファイルシステム管理 +
メモ：link:{path_session5}[こちら] +

7．シェル、スクリプト、データ管理 +
メモ：link:{path_session7}[こちら] +

6、8．ボーナスレクチャ2 +
|===


== 遭遇した課題
====
* VirtualBoxでCentOS7のGuestAdditionsが動作しない +
➞解決（詳細はlink:{path_session2}[セッション2のメモ]上に記述）

* vi 編集時に矢印ボタン（上下左右）、バックペースボタンが動作しない +
➞解決（詳細はlink:{path_session7}[セッション7のメモ]上に記述）
====


<<<

== 学習したと感じたもの
====
1．パッケージ管理システムの知識 +
2．ファイルシステム管理の知識 +
3．ファイルシステムの標準（各フォルダに保存するファイルの種類） +
4．bash構成ファイル +
====

== 感想
====
* バックグランド： +
大学院時代にubuntuを使用して研究したことがあり、前のジョブでもLinuxサーバーを触っていたため、Linux基礎知識、環境構築、基本のコマンド操作などの経験はある。そのため、復習となる部分が半分くらいとなる。 +

* 講義について： +
1．ビデオ講義、それに操作の部分も少なかったのため、眠きと戦う日々だった。毎日3杯コーヒーを飲んだ気がする。 +
2．FSチームと同じく、講義の後半は試験対策の部分が多かったため、仕事に役に立つ部分が少ない気がする。 +
3．講義上ではubuntu 14を使用するが、最新のバージョンは18だったので、そのまま18をインストールした。初心者ならば混乱するかもしれない。 +
4．練習問題が少なく、簡単なステップで終了する問題もあったため、物足りないと感じた。 +
====

== アドバイス
====
* 直接LPIC1試験を挑戦するようなミッションならばいいかもしれない。具体的な構造は下記となる。 +
1．virtualbox、ubuntuなどの環境構築手順はBootCamp前回の受講者から作成し、次のBootCamp受講者に渡す。 +
2．LPIC1の学習教材（教科書、問題集など）を購入し、試験勉強に集中する。コマンドの実行など、教科書にある操作の部分では構築した環境で試す。
====

