:toc: left
:sectnums:
:toc-title: 目次
:toclevels: 1
:icons: font
:author: チームIES　羅
:revnumber: 1.0



= Session5、ファイルシステムの管理

== 概要
====
* 項目 +
1．ファイルシステムの確認、管理 +
2．ディスククォータの管理 +
3．FHSとファイルの探査 +
====

== ファイルシステムの確認

=== df(disk free)コマンド
====
* df [オプション] ＜ファイル名＞ +
* オプション +
-i ：iノード使用状況の表示 +
-h ：M（メガバイト）、G（ギガバイト）の表示 +
-T ：ファイルシステムの表示 +
====

=== du(disk free)コマンド
====
* du [オプション] ＜ファイル名＞ +
* オプション +
-a ：ディレクトリと全てのディスクの表示 +
-s ：合計のみを表示 +
-h ：M（メガバイト）、G（ギガバイト）の表示 +
====


<<<


=== fsck(file system check)コマンド
====
* fsck [オプション] ＜デバイスファイル＞ +
* オプション +
-t ：ファイルシステム(ext2, ext3, ext4など) +
-c ：ファイルシステム作成時に不良ブロックを調査 +
-b ：バックアップスーパーブロックを指定して復旧 +
-n ：コマンド実行確認 +
====


=== dumpe2fsコマンド
====
* dumpe2fs [オプション] ＜デバイスファイル＞
* option +
-b ：不良ブロックを表示 +
-h ：スーパーブロックの表示
====

=== スーパーブロックのバックアップ場所
====
* 1024 ：8193
* 2048 ：16834
* 4096 ：32768
====

<<<

=== debugfsコマンド
====
* debugfs [option] <device file>
* options +
-b ：ブロックサイズ専用 +
-c ：読み取り専用 +
-R コマンド：対話式ではなく、指定したコマンドを実行する +
-n ：読み取り +

* debugfs [-R コマンド] <device file>
* options +
ls +
cat +
cd +
-n +
chroot ：rootディレクトリの変更 +
pwd +
close +
quit ：debugfsからEXITする
====



=== tune2fsコマンド
====
* tune2fs [option] <device file>
* options +
-c ：ファイルシステムチェック実行までのマウント回数 +
-i d, m, w ：ファイルシステムチェック間隔 +
-j ：ext3ジャーナル（変更履歴）を追加 +
-I ：スーパーブロック情報の表示 +
-L ：ファイルシステムにボリューム名を付ける +
====


=== XFS管理ツール
====
* XFS (ジャーナリングシステム、500B.CentOS 7 /RHEL 7で標準) +
* tools
xfsinfo ：XSFファイルシステムに関する情報表示 + 
xfs_metadump ：XFSファイルシステムのメタデータをファイル保存 +
xfs_mdrestore ：xfs_metadumeイメージを復元 +

* 確認はCentOSで
====

<<<

== ディスククォータの管理

=== クォータとは 
====
* Linux上でユーザーが使えるディスク領域の制御仕組み

* クォータの制御方法：ハードリミット、ソフトリミット +
警告領域を設けて、ハード制限まで利用可能

* Type +
1．｛ユーザー単位・グループ単位｝X｛ハード制限・ソフト制限｝ +
2．猶予期間
====

=== クォータの設定
====
* クォータリモートでマウント +
/etc/fstabにて、usrquotaまたはgrpquotaを指定してマウンド
====

=== クォータスペースの作成
====
* quotacheckコマン（クオータの初期化）
* options +
-b ：バックアップファイルの作成 +
-c ：新規作成 +
-v ：詳細表示 +
====



=== クォータ設定（edquotaコマンド）
====
* edquota [option] filesystem
* options +
-u ：ユーザー名 +
-g ：グループ名 +
-t ：ソフトリミット猶予期間 +
====


<<<


=== クォータの有効化
====
* quotaon [option] falesystem
* quotaoff [option] falesystem
* options +
-a ：全ファイルシステムの設定を有効化 +
-u ：ユーザー名を有効化 +
-g ：グループ名を有効化 +
-v ：メッセージの詳細表示 +
====


=== クォータ状況の確認
====
* repquota (report quota)コマンド +
repoquota [-a] <file or system> +
-a ：全システムを対象

* quota {-u, -g}：ユーザー・グループのクォータ設定表示
====





== FHS（File Hierarchy Srtandard）
====
* ファイルシステムの標準 +
/ +
/bin +
/sbin +
/var +
/etc +
/dev +
/tmp +
/boot +
/proc +
/usr +
/home +
/root +
/mnt +
/media +
/opt +
/srv +
====

<<<

=== /usr（読み込み専用ファイル）
====
* ディレクトリ +
/usr/bin ：通常使用するコマンド +
/usr/sbin ：システム管理コマンド +
/usr/lib ：アプリケーション管理コマンド +
/usr/include ：C/C++のインクルード・ヘッダファイル +
/usr/loacal ：管理者独自にインストールしたアプリ +
/usr/share ：アーキテクチャやLinuxのバージョン非依存のファイル +
/usr/share/man ：manコマンドで表示するオンラインマニュアルファイルを配置 +
/usr/src ：カーネルのソース +
/user/X11R6 ：X Windows Syatem関連のファイル +
====


=== /usr/local（読み込み専用ファイル）
====
* ディレクトリ +
/usr/local/bin ：通常使用するコマンド +
/usr/local/sbin ：システム管理コマンド +
/usr/local/lib ：アプリケーション管理コマンド +
/usr/local/include ：C/C++のインクルード・ヘッダファイル +
/usr/local/loacal ：管理者独自にインストールしたアプリ +
/usr/local/share ：アーキテクチャやLinuxのバージョン非依存のファイル +
/usr/local/share/man ： 任意マニュアルファイルを配置 +
/usr/local/src ：任意ソースコード +
====

=== ファイルの探査
====
* find 
* locate
* whereis
* which
* type
====

<<<

=== locate
====
* データベースから探査
* updatedbコマンドを実行する必要がある +
1．PRUNFES
2．PRUNEPATHS
====



=== whereis
====
* whereis [option] <file name>
* options +
-b ：バイナリのみ +
-m ：manページのみ +
-s ：ソースのみ +
====

=== which
====
* which <path> [option] <command name> +
PATHを順に探査する +
組み込みコマンドは不明 +
====

=== type
====
* type <command name> +
実行ファイルや、組み込みコマンドなどの配置場所 +
====

<<<

== 練習

=== whereis
====
* 下記コマンドを実行 +
whereis ls
====
結果： +
|===
image:/pic/01.png[]
|===

=== which
====
* 下記コマンドを実行 +
1．which ls +
2．which cd +
====
|===
結果： +
image:/pic/02.png[]
|===

<<<

=== type
====
* 下記コマンドを実行 +
1．type ls +
2．type cd +
====
|===
結果： +
image:/pic/03.png[]
|===